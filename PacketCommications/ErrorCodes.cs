﻿using System;

namespace PacketCommications

{
    public static class Errors
    {
        public const int InstrumentNotConnected = -6003;               // instrument is not connected
        public const int MissingInstrumentOption = -6010;           // a option in the instrument is not present
        public const int DUTConsoleOpenError = -6020;               // error opening the sensor console port
        public const int DUTConsoleNotOpen = -6021;                 // trying to use the sensor console port but it is not open
        public const int DUTConsoleWriteError = -6022;              // error writing to the sensor console
        public const int OptionFormatWrong = -6023;                 // the format of one of the options is wrong

        // DUT console errors. com errors are a DUT error, not a system error
        public const int dutUnexpectedCmdReturn = 300;
        public const int dutCommandTimeout = 301;
        public const int dutCmdResponseStatusFailed = 302;
        public const int dutCmdResponsePayloadLenWrong = 303;
        public const int dutCmdPacketCheckSumError = 304;
        public const int dutCmdResponseHeaderLenWrong = 305;
        public const int dutCmdPacketFormatError = 306;
        public const int dutCmdMsgTypeNotDVTMAN = 307;
        public const int dutFailedTest = 308;
    }
}
